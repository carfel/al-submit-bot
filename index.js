const config = require('./config.js');
const mysql = require('mysql2/promise');
const Discord = require('discord.js');
const client = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });

var interfaceConnection;
var handleInterfaceConnection = async function () {
    interfaceConnection = await mysql.createConnection(config.db_config);
    interfaceConnection.on('error', function (err) {
        console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            handleInterfaceConnection();
        } else {
            handleInterfaceConnection();//usually, throw err;
        }
    });
    return;
}

var queryDatabase = async function (connection, query, params = []) {
    try {
        const [rows, fields] = await connection.execute(query, params);
        return rows;
    } catch (error) {
        console.log(error);
        console.log(query);
        console.log(params);
        return [];
    }
}

var scoreboard_channel_id = config.scoreboard_channel_id;
var submit_game_channel_id = config.submit_game_channel_id;
var pending_submits_channel_id = config.pending_submits_channel_id;
var register_channel_id = config.register_channel_id;
var guild_id = config.guild_id;
var token = config.token;

var scoremessage = "```\n";
scoremessage += "+--------------------------------+\n";
scoremessage += "|            Scoreboard          |\n";
scoremessage += "+------------+---------+---------+\n";
scoremessage += "|  Team      |  Score  |  W/L    |\n";
scoremessage += "+------------+---------+---------+\n";


var getOpponent = async (roles, message) => {
    try {
        let opponent = false;
        var temp = 0;
        await roles.each(role => {
            let role_index = message.content.indexOf(role.id);
            if(role_index > temp) {
                temp = role_index;
                opponent = role;
            }
        });
        return opponent;
    } catch(error) {
        console.log(error);
        return false;
    }
}

var getMyteam = async (roles, message) => {
    try {
        let opponent = false;
        var temp = null;
        await roles.each(role => {
            let role_index = message.content.indexOf(role.id);
            if(role_index > -1 && (role_index < temp || temp == null)) {
                temp = role_index;
                opponent = role;
            }
        });
        return opponent;
    } catch(error) {
        console.log(error);
        return false;
    }
}

client.on('messageReactionAdd', async (reaction, user) => {
    try {
        
        if (reaction.partial) {
            try {
                await reaction.fetch();
            } catch (error) {
                console.error('Something went wrong when fetching the message: ', error);
                return;
            }
        }
        
        if (reaction.emoji.name == "✅") {
            console.log("Checking if user has opponent role");
            let opponent_role = await getOpponent(reaction.message.mentions.roles, reaction.message);
            if(!opponent_role) return;
            let hasRole = await playerHasRole(user, opponent_role);
            if (hasRole) {
                console.log("Player has role!");
                await updateScore(reaction.message);
                await reaction.message.delete();
            } else {
                console.log("Player does not have role!");
            }
        } else if (reaction.emoji.name == "❌") {
            console.log("Checking if user has opponent role");
            let opponent_role = await getOpponent(reaction.message.mentions.roles, reaction.message);
            let myteam_role = await getMyteam(reaction.message.mentions.roles, reaction.message);
            let hasRole = await playerHasRole(user, opponent_role);
            let hasMyRole = await playerHasRole(user, myteam_role);
            if (hasRole || hasMyRole) {
                console.log("Player has role!");
                await reaction.message.delete();
                if (hasRole) {
                    let submituser = reaction.message.mentions.users.first();
                    submituser.send("Your submit vs " + opponent_role.name + " was denied by <@" + user + ">");
                }
            } else {
                console.log("Player does not have role!");
            }
        }
    } catch (error) {
        console.log(error);
    }
});

client.on('ready', async () => {
    try {
        var channel = await client.channels.fetch(scoreboard_channel_id);
        if (!channel) return;

        var scoreboard_messages = await channel.messages.fetch();
        if (scoreboard_messages.size == 1) {
            console.log("Edit scoreboard message");
            var message = await scoreboard_messages.first();
            var scoreboard = await loadScoreboard();
            message.edit(scoremessage + scoreboard + "```");
        } else if (scoreboard_messages.size > 1) {
            console.log("Clear and Initialize Scoreboard");
            await scoreboard_messages.each(async message => {
                await message.delete();
            });
            var scoreboard = await loadScoreboard();
            message.edit(scoremessage + scoreboard + "```");
        } else {
            console.log("Initialize Scoreboard");
            var scoreboard = await loadScoreboard();
            message.edit(scoremessage + scoreboard + "```");
        }
    } catch (error) {
        console.log(error);
    }
});

client.on('message', async msg => {
    try {
        if (msg.channel.id == submit_game_channel_id && msg.content.startsWith('!submit')) {
            console.log("New submit!");
            try {
                //example: !submit @nothFPS @felix 1-0
                msg.content = msg.content.trim();
                if (msg.mentions.roles.size == 2) {
                    var team1 = await getMyteam(msg.mentions.roles, msg);
                    var team2 = await getOpponent(msg.mentions.roles, msg);
                    var score = msg.content.split(" ")[3];
                    var team1score = Number(score.split("-")[0]);
                    var team2score = Number(score.split("-")[1]);
                    if (isNaN(team1score) || isNaN(team2score)) {
                        await msg.reply("Submit failed\nPossible reasons:\nYour team is not registered\nOpponent team is not registered\nFaulty submit format, example: !submit @you @opponent 1-0");
                        return false;
                    }
                    let hasRole = await playerHasRole(msg.author, team1);
                    if (!hasRole) {
                        await msg.reply("Submit failed\nPossible reasons:\nYour team is not registered\nOpponent team is not registered\nFaulty submit format, example: !submit @you @opponent 1-0");
                        return false;
                    }
                    let success = await addToSubmit(team1, team2, team1score, team2score, msg.author);
                    if (success) {
                        await msg.reply("Successfully added submit, please check submits channel.");
                    } else {
                        await msg.reply("Submit failed\nPossible reasons:\nYour team is not registered\nOpponent team is not registered\nFaulty submit format, example: !submit @you @opponent 1-0");
                    }
                } else if (msg.mentions.users.size == 2) {
                    var player1 = await getMyteam(msg.mentions.users, msg);
                    var player2 = await getOpponent(msg.mentions.users, msg);
                    var score = msg.content.split(" ")[3];
                    var team1score = Number(score.split("-")[0]);
                    var team2score = Number(score.split("-")[1]);
                    let team1 = await getPlayerRole(player1);
                    let team2 = await getPlayerRole(player2);
                    if (team1 == null || team2 == null || isNaN(team1score) || isNaN(team2score)) {
                        await msg.reply("Submit failed\nPossible reasons:\nYour team is not registered\nOpponent team is not registered\nFaulty submit format, example: !submit @you @opponent 1-0");
                        return false;
                    }
                    let hasRole = await playerHasRole(msg.author, team1);
                    if (!hasRole) {
                        await msg.reply("Submit failed\nPossible reasons:\nYour team is not registered\nOpponent team is not registered\nFaulty submit format, example: !submit @you @opponent 1-0");
                        return false;
                    }
                    let success = await addToSubmit(team1, team2, team1score, team2score, msg.author);
                    if (success) {
                        await msg.reply("Successfully added submit, please check submits channel.");
                    } else {
                        await msg.reply("Submit failed\nPossible reasons:\nYour team is not registered\nOpponent team is not registered\nFaulty submit format, example: !submit @you @opponent 1-0");
                    }
                } else {
                    await msg.reply("Submit failed\nPossible reasons:\nYour team is not registered\nOpponent team is not registered\nFaulty submit format, example: !submit @you @opponent 1-0");
                }
            } catch (error) {
                console.log(error);
                await msg.reply("Submit failed\nPossible reasons:\nYour team is not registered\nOpponent team is not registered\nFaulty submit format, example: !submit @you @opponent 1-0");
            }
        } else if (msg.channel.id == register_channel_id && msg.content.startsWith('!register')) {
            //example: !register @GodS
            try {
                if (msg.mentions.roles.size == 1) {
                    var role = msg.mentions.roles.first();
                    let res = await queryDatabase(interfaceConnection, "SELECT * FROM teams WHERE team_id = ?", [role.id]);
                    if (res.length == 0) {
                        await queryDatabase(interfaceConnection, "INSERT INTO teams (team_id, name, score) VALUES (?,?,0)", [role.id, role.name]);
                        msg.reply(role.name + " registered successfully!");
                        var scoreboard = await loadScoreboard();
                        var channel = await client.channels.fetch(scoreboard_channel_id);
                        if (!channel) return;
                        var scoreboard_messages = await channel.messages.fetch();
                        var message = await scoreboard_messages.first();
                        await message.edit(scoremessage + scoreboard + "```");
                    } else msg.reply(role.name + " is already registered!");
                }
            } catch (error) {
                console.log(error);
            }
        }
    } catch (error) {
        console.log(error);
    }
});

var updateScore = async (msg) => {
    try {
        var team1 = await getMyteam(msg.mentions.roles, msg);
        var team2 = await getOpponent(msg.mentions.roles, msg);
        var score = msg.content.split(" ")[3];
        var team1score = Number(score.split("-")[0]);
        var team2score = Number(score.split("-")[1]);
        if (team1score > 0) {
            await queryDatabase(interfaceConnection, "UPDATE teams SET win = win + 1, score = score + ? WHERE team_id = ?", [team1score, team1.id]);
            await queryDatabase(interfaceConnection, "UPDATE teams SET lose = lose + 1, score = score + ? WHERE team_id = ?", [team2score, team2.id]);
            var scoreboard = await loadScoreboard();
            var channel = await client.channels.fetch(scoreboard_channel_id);
            if (!channel) return;
            var scoreboard_messages = await channel.messages.fetch();
            var message = await scoreboard_messages.first();
            await message.edit(scoremessage + scoreboard + "```");
        } else if (team2score > 0) {
            await queryDatabase(interfaceConnection, "UPDATE teams SET win = win + 1, score = score + ? WHERE team_id = ?", [team2score, team2.id]);
            await queryDatabase(interfaceConnection, "UPDATE teams SET lose = lose + 1, score = score + ? WHERE team_id = ?", [team1score, team1.id]);
            var scoreboard = await loadScoreboard();
            var channel = await client.channels.fetch(scoreboard_channel_id);
            if (!channel) return;
            var scoreboard_messages = await channel.messages.fetch();
            var message = await scoreboard_messages.first();
            await message.edit(scoremessage + scoreboard + "```");
        }
        return;
    } catch (error) {
        console.log(error);
        return;
    }
}

var loadScoreboard = async () => {
    try {
        let scoreboard_result = await queryDatabase(interfaceConnection, "SELECT * FROM teams ORDER BY score DESC");
        var scoreboard_text = "";
        for (let i = 0; i < scoreboard_result.length; i++) {
            const team = scoreboard_result[i];
            var team_name = team.name;
            var dotsNeeded = 8 - team_name.length;
            for (let awd = 0; awd < dotsNeeded; awd++) {
                team_name += " ";
            }
            var score = team.score.toString();
            var scoreDotsNeeded = 5 - score.length;
            for (let awd = 0; awd < scoreDotsNeeded; awd++) {
                score += " ";
            }

            var wl = team.win + "/" + team.lose;
            var wlDotsNeeded = 5 - wl.length;
            for (let awd = 0; awd < wlDotsNeeded; awd++) {
                wl += " ";
            }
            scoreboard_text += "|  " + team_name + "  |  " + score + "  |  " + wl + "  |\n";
            scoreboard_text += "+------------+---------+---------+\n";
        }
        return scoreboard_text;
    } catch (error) {
        console.log(error);
        return "";
    }
}

var getPlayerRole = async (player) => {
    try {
        var foundRole = null;
        let teams = await queryDatabase(interfaceConnection, "SELECT * FROM teams;")
        var guild = await client.guilds.fetch(guild_id);
        if (!guild) return;
        var member = await guild.members.fetch(player.id)
        for (let i = 0; i < teams.length; i++) {
            const team = teams[i];
            var team_id = team.team_id;
            var potential_role = await member.roles.cache.get(team_id);
            if (potential_role) foundRole = potential_role;
        }
        return foundRole;
    } catch (error) {
        console.log(error);
        return null;
    }
}

var playerHasRole = async (player, role) => {
    try {
        var foundRole = false;
        var guild = await client.guilds.fetch(guild_id);
        if (!guild) return;
        var member = await guild.members.fetch(player.id)

        var foundRole = await member.roles.cache.get(role.id);
        if (foundRole) return foundRole;
        else return false;
    } catch (error) {
        console.log(error);
        return false;
    }
}

var addToSubmit = async (team1, team2, team1score, team2score, author) => {
    try {
        var channel = await client.channels.fetch(pending_submits_channel_id);
        if (!channel) return false;
        console.log(team1.name + ": " + team1score);
        console.log(team2.name + ": " + team2score);
        let message = await channel.send("<@&" + team1.id + "> vs <@&" + team2.id + "> " + team1score + "-" + team2score + " (Submitted by <@" + author + ">)");
        await message.react("✅");
        await message.react("❌");
        return true;
    } catch (error) {
        console.log(error);
        return false;
    }
}

var init = async () => {
    await handleInterfaceConnection();
    client.login(token);
}
init();